// Iteración #5: Calcular promedio de strings
// Crea una función que reciba por parámetro un array y cuando es un valor number lo sume y de lo contrario cuente la longitud del string y lo sume. Puedes usar este array para probar tu función:

const mixedElements = [6, 1, 'Rayo', 1, 'vallecano', '10', 'upgrade', 8, 'hub', true];
let sumString = 0;
let sumNumbers = 0;

function averageWord(param) {
    for(value of param){
        if(typeof value === 'number' || typeof value === 'string'){ //Control errores , valor booleano añadido al array.
            typeof value == 'number' ? sumNumbers += value : sumString += value.length;
        }        
    }

    return "La suma de los numeros es: " + sumNumbers + "\n y la suma de las palabras es: " +sumString;
}

console.log(averageWord(mixedElements));