// Iteration #8: Contador de repeticiones
// Crea una función que nos devuelva el número de veces que se repite cada una de las palabras que lo conforma. Puedes usar este array para probar tu función:
/*
Solucion
code = 4
repeat = 0
eat = 0
sleep = 2
enjoy = 2
upgrade = 0
*/


const counterWords = [
    'code',
    'repeat',
    'eat',
    'sleep',
    'code',
    'enjoy',
    'sleep',
    'code',
    'enjoy',
    'upgrade',
    'code'
];

function repeatCounter(param) {
    let repeat = [];
    for (let i = 0; i < param.length; i++) {
        if (!repeat[param[i]]) {
            repeat[param[i]] = 1;
        } else {
            repeat[param[i]] += 1;
        }
    }
    return repeat;
};

console.log(repeatCounter(counterWords));