// Iteración #7: Buscador de nombres
// Crea una función que reciba por parámetro un array y el valor que desea comprobar que existse dentro de dicho array - comprueba si existse el elemento, en caso que existsan nos devuelve un true y la posición de dicho elemento y por la contra un false. Puedes usar este array para probar tu función:

const nameFinder = [
    'Peter',
    'Steve',
    'Tony',
    'Natasha',
    'Clint',
    'Logan',
    'Xabier',
    'Bruce',
    'Peggy',
    'Jessica',
    'Marc'
];



function finderName(param, search) {
    let position = 0;
    let exists = false;
    for (value of param) {
        if (value === search) {
            exists = true;
            console.log("El nombre " + exists + " y está en la posición " + (position+1));
            return;
        }
        position++;
    }
    console.log("El nombre no existe.")
}

finderName(nameFinder, 'Tony');