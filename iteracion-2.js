// Iteración #2: Buscar la palabra más larga
// Completa la función que tomando un array de strings como argumento devuelva el más largo, en caso de que dos strings tenga la misma longitud deberá devolver el primero.
// Puedes usar este array para probar tu función:

const avengers = ['Hulk', 'Thor', 'IronMan', 'Captain A.', 'Spiderman', 'Captain M.'];

// function findLongestWord() {
//     let total= avengers[0];

//     for(let key of avengers){
//         if (key.length >= total.length){
//             total = key;
//         }
//     }
    
//     return total;
// }

// console.log(findLongestWord());

let longerWord = '';
function findLongestWord(param){

    param.forEach(function(element){
        if (longerWord.length < element.length){
            longerWord = element;
        }
    });

    return longerWord;
}

console.log(findLongestWord(avengers));