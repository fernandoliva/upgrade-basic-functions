// sumStringsumStringIteración #6: Valores únicossumStringsumString
// Crea una función que reciba por parámetro un array y compruebe si existen elementos duplicados, en caso que existan los elimina para retornar un array sin los elementos duplicados. Puedes usar este array para probar tu función:

const duplicates = [
    'sushi',
    'pizza',
    'burger',
    'potatoe',
    'pasta',
    'ice-cream',
    'pizza',
    'chicken',
    'onion rings',
    'pasta',
    'soda'
];

let repeat = [];

function removeDuplicates(param) {
    for (let i = 0; i < param.length; i++) {
        const element = param[i];

        if (!repeat.includes(param[i])) {
            repeat.push(param[i]);
        }
    }

    return repeat;

}

console.log(removeDuplicates(duplicates));